#!/bin/bash

#date
echo "Starting the simulation:"
echo "Current time is:"
date

P=0

#$string="test" 
for P in 10. 20. 40. 60. 80. 100
do 

    string="nu34_P${P}W_Q30000_1e8event_range800kHz_300mbar_omega_const"

    mkdir ${string}
    mkdir ${string}/log
    mkdir ${string}/result
    
    for i in {0..999..1}
    do
    
	printf -v i "%d" $i
	
	echo i=${i}
	
	sleep 0.2
	
	bsub -J toyMC_${string}_${i} -o ${string}/jobs.log "root -l 'toyMC.C(${i},\"${string}\",${P})'&>${string}/log/${i}.log&"
        
	echo "Job submitted"
	echo "Current time is:"
	date
	echo
	
    done

done

echo "All jobs submitted"
echo "Current time is:"
date


